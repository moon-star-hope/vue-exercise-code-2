import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Home from '../views/Home.vue'
import Welcome from '../views/welcome.vue'
import Users from '../views/users/index.vue'
import Roles from '../views/power/Roles.vue'
import Rights from '../views/power/Rights.vue'
import Cate from '../views/goods/cate.vue'
Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login', component: Login },
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [
      { path: '/welcome', component: Welcome },
      { path: '/users', component: Users },
      { path: '/roles', component: Roles },
      { path: '/rights', component: Rights },
      { path: '/categories', component: Cate }
    ]
  }
]
const router = new VueRouter({
  routes
})

// 设置导航首位
router.beforeEach((to, from, next) => {
  // to 将要访问的路径
  // form 表示从那个路径跳转而来
  // next表示放行 next('/login') 强制跳转
  if (to.path === '/login') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})
export default router
